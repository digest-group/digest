package com.bignerdranch.android.digest

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bignerdranch.android.digest.presentation.OptionsActivity
import com.bignerdranch.android.digest.presentation.account.AccountActivity
import com.bignerdranch.android.digest.presentation.news.list.NewsListActivity
import com.bignerdranch.android.digest.presentation.weather.list.WeatherListActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class OptionsActivityTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(OptionsActivity::class.java)

    @Before
    fun init(){
        Intents.init()
    }

    @Test
    fun onWeatherButtonClicked(){
        Espresso.onView(ViewMatchers.withId(R.id.weather_button)).perform(ViewActions.click())
        Intents.intended(IntentMatchers.hasComponent(WeatherListActivity::class.java.name))
    }

    @Test
    fun onNewsButtonClicked(){
        Espresso.onView(ViewMatchers.withId(R.id.news_button)).perform(ViewActions.click())
        Intents.intended(IntentMatchers.hasComponent(NewsListActivity::class.java.name))
    }

    @Test
    fun onAccountButtonClicked(){
        Espresso.onView(ViewMatchers.withId(R.id.account_button)).perform(ViewActions.click())
        Intents.intended(IntentMatchers.hasComponent(AccountActivity::class.java.name))
    }

    @After
    fun destroy(){
        Intents.release()
    }
}