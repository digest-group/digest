package com.bignerdranch.android.digest

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bignerdranch.android.digest.presentation.OptionsActivity
import com.bignerdranch.android.digest.presentation.login.MainActivity
import com.bignerdranch.android.digest.presentation.registration.SignUpActivity
import com.google.firebase.auth.FirebaseAuth
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun init() {
        Intents.init()
        IdlingRegistry.getInstance()
            .register(CountingIdlingResourceSingleton.countingIdlingResource)
        FirebaseAuth.getInstance().signOut()
    }

    @Test
    fun loginButtonClicked() {
        onView(withId(R.id.email_edit_text)).perform(
            typeText("checkMail@gmail.com"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.password_edit_text)).perform(
            typeText("checkPassword"),
            closeSoftKeyboard()
        )
        onView(withId(R.id.login_button)).perform(click())
        intended(hasComponent(OptionsActivity::class.java.name))
    }

    @Test
    fun signUpButtonClicked() {
        onView(withId(R.id.sign_up_button)).perform(click())
        intended(hasComponent(SignUpActivity::class.java.name))
    }

    @After
    fun destroy() {
        Intents.release()
        IdlingRegistry.getInstance()
            .unregister(CountingIdlingResourceSingleton.countingIdlingResource)
    }

}