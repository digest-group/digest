package com.bignerdranch.android.digest

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bignerdranch.android.digest.presentation.account.AccountActivity
import com.bignerdranch.android.digest.presentation.login.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AccountActivityTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(AccountActivity::class.java)

    @Before
    fun init(){
        Intents.init()
    }

    @Test
    fun onLogOutButtonClicked(){
        Espresso.onView(ViewMatchers.withId(R.id.logout_button)).perform(ViewActions.click())
        Intents.intended(IntentMatchers.hasComponent(MainActivity::class.java.name))
    }

    @After
    fun destroy(){
        Intents.release()
    }
}