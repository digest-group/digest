package com.bignerdranch.android.digest

import CountingIdlingResourceSingleton
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.PerformException
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollTo
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.GrantPermissionRule
import com.bignerdranch.android.digest.presentation.OptionsActivity
import com.bignerdranch.android.digest.presentation.news.list.NewsListActivity
import com.bignerdranch.android.digest.presentation.weather.details.WeatherDetailsActivity
import com.bignerdranch.android.digest.presentation.weather.list.WeatherListActivity
import kotlinx.coroutines.test.withTestContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class WeatherListActivityTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(WeatherListActivity::class.java)
    @get:Rule
    val grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION)

    @Before
    fun init() {
        Intents.init()
        IdlingRegistry.getInstance()
            .register(CountingIdlingResourceSingleton.countingIdlingResource)
    }

    @Test
    fun isWeatherListVisible_onAppLaunch(){
        onView(withId(R.id.cities_list)).check(matches(isDisplayed()))
    }

    @Test
    fun onFindCity() {
        onView(withId(R.id.city_edit_text)).perform(
            ViewActions.typeText("Paris"),
            ViewActions.closeSoftKeyboard()
        )
        onView(withId(R.id.find_button)).perform(click())
        intended(IntentMatchers.hasComponent(WeatherDetailsActivity::class.java.name))
    }

    @Test
    fun favoritesClicked() {
        onView(withId(R.id.city_edit_text)).perform(
            ViewActions.typeText("Paris"),
            ViewActions.closeSoftKeyboard()
        )
        onView(withId(R.id.find_button)).perform(click())
        onView(withId(R.id.add_to_favorite_button))
            .perform(click())
        onView(withId(R.id.add_to_favorite_button))
            .check(matches(isChecked()))
    }

    @Test
    fun favoritesAvailable() {
        onView(withText("FAVORITES"))
            .perform(click())
    }

    @Test
    fun detailsBackClicked() {
        onView(withId(R.id.city_edit_text)).perform(
            ViewActions.typeText("Paris"),
            ViewActions.closeSoftKeyboard()
        )
        onView(withId(R.id.find_button)).perform(click())
        onView(withText("BACK"))
            .perform(click())
    }

    @Test(expected = PerformException::class)
    fun itemWithText_doesNotExist() {
        onView(withId(R.id.cities_list))
            .perform(
                scrollTo<RecyclerView.ViewHolder>(
                    hasDescendant(
                        withText("Not in the list")
                    )
                )
            )
    }

    @Test
    fun onBackClicked(){
        onView(withId(R.id.back_button_weather_list)).perform(click())
        intended(IntentMatchers.hasComponent(OptionsActivity::class.java.name))
    }

    @After
    fun destroy() {
        Intents.release()
        IdlingRegistry.getInstance()
            .unregister(CountingIdlingResourceSingleton.countingIdlingResource)
    }
}