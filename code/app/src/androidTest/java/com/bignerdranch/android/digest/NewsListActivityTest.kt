package com.bignerdranch.android.digest

import CountingIdlingResourceSingleton
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.PerformException
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollTo
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bignerdranch.android.digest.presentation.OptionsActivity
import com.bignerdranch.android.digest.presentation.news.details.NewsDetailsActivity
import com.bignerdranch.android.digest.presentation.news.list.NewsListActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class NewsListActivityTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(NewsListActivity::class.java)

    @Before
    fun init() {
        Intents.init()
        IdlingRegistry.getInstance()
            .register(CountingIdlingResourceSingleton.countingIdlingResource)
    }

    @Test
    fun isNewsListVisible_onAppLaunch(){
        onView(withId(R.id.news_list)).check(matches(isDisplayed()))
    }

    @Test(expected = PerformException::class)
    fun itemWithText_doesNotExist() {
        onView(withId(R.id.news_list))
            .perform(
                scrollTo<RecyclerView.ViewHolder>(
                    hasDescendant(
                        withText("Not in the list")
                    )
                )
            )
    }

    @Test
    fun favoritesClicked() {
        onView(withId(R.id.news_list))
            .perform(click())
        onView(withId(R.id.add_to_favorite_button))
            .perform(scrollTo(), click())
        onView(withId(R.id.add_to_favorite_button))
            .check(matches(isChecked()))
    }

    @Test
    fun favoritesAvailable() {
        onView(withText("FAVORITES"))
            .perform(click())
    }

    @Test
    fun detailsBackClicked() {
        onView(withId(R.id.news_list))
            .perform(click())
        onView(withId(R.id.back_button_details))
            .perform(scrollTo(), click())
        intended(IntentMatchers.hasComponent(NewsListActivity::class.java.name))
    }

    @Test
    fun onBackClicked(){
        onView(withId(R.id.back_button_news_list)).perform(click())
        intended(IntentMatchers.hasComponent(OptionsActivity::class.java.name))
    }

    @After
    fun destroy() {
        Intents.release()
        IdlingRegistry.getInstance()
            .unregister(CountingIdlingResourceSingleton.countingIdlingResource)
    }
}