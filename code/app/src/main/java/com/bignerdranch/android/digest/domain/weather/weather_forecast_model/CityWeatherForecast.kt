package com.bignerdranch.android.digest.domain.weather.weather_forecast_model

data class CityWeatherForecast(
    val list: List<Forecast>
)