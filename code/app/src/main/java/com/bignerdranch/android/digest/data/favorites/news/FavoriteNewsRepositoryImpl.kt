package com.bignerdranch.android.digest.data.favorites.news

import com.bignerdranch.android.digest.domain.news.FavoriteNewsRepository
import com.bignerdranch.android.digest.domain.news.news_model.Article
import io.reactivex.Completable
import io.reactivex.Single

class FavoriteNewsRepositoryImpl(private val favoriteNewsDataSource: FavoriteNewsDataSource) :
    FavoriteNewsRepository {

    override fun getAllFavoriteNews(): Single<List<Article>> =
        favoriteNewsDataSource.getAllFavoriteNews()

    override fun insertFavoriteNews(favoriteArticle: Article): Completable =
        favoriteNewsDataSource.insertFavoriteNews(favoriteArticle)

    override fun deleteFavoriteNews(favoriteArticle: Article): Completable =
        favoriteNewsDataSource.deleteFavoriteNews(favoriteArticle)

    override fun getFavoriteNews(title: String): Single<Article> =
        favoriteNewsDataSource.getFavoriteNews(title)
}