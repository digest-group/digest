package com.bignerdranch.android.digest.data.favorites.news

import com.bignerdranch.android.digest.domain.news.news_model.Article
import io.reactivex.Completable
import io.reactivex.Single

class FavoriteNewsLocalDataSource(private val favoriteNewsDao: FavoriteNewsDao) :
    FavoriteNewsDataSource {

    override fun getAllFavoriteNews(): Single<List<Article>> =
        favoriteNewsDao.getAllFavoriteArticles()

    override fun insertFavoriteNews(favoriteArticle: Article): Completable =
        favoriteNewsDao.insertArticle(favoriteArticle)

    override fun deleteFavoriteNews(favoriteArticle: Article): Completable =
        favoriteNewsDao.deleteFavoriteArticle(favoriteArticle)

    override fun getFavoriteNews(title: String): Single<Article> =
        favoriteNewsDao.getFavoriteArticle(title)
}