package com.bignerdranch.android.digest.presentation.adapters.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bignerdranch.android.digest.databinding.ItemNewsBinding
import com.bignerdranch.android.digest.domain.news.news_model.Article
import com.squareup.picasso.Picasso

class NewsAdapter(private val onItemClick: (Article) -> Unit): RecyclerView.Adapter<NewsAdapter.NewsHolder>() {
    var breakingNews: List<Article> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemNewsBinding = ItemNewsBinding.inflate(layoutInflater, parent, false)
        return NewsHolder(itemNewsBinding, onItemClick)
    }

    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        val article = breakingNews[position]
        holder.bind(article)
    }

    override fun getItemCount(): Int = breakingNews.count()

    class NewsHolder(private val itemNewsBinding: ItemNewsBinding,
                        private val onItemClick: (Article) -> Unit) : RecyclerView.ViewHolder(itemNewsBinding.root) {

        fun bind(article: Article) {
            itemNewsBinding.newsTitleText.text = article.title
            val urlToImage = article.urlToImage
            Picasso.with(itemView.context).load(urlToImage)
                .fit()
                .centerInside()
                .into(itemNewsBinding.newsImage)
            itemView.setOnClickListener {
                onItemClick(article)
            }
        }
    }
}