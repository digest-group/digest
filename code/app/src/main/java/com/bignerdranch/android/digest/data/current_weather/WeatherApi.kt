package com.bignerdranch.android.digest.data.current_weather

import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.current_weather_model.Response
import com.bignerdranch.android.digest.domain.weather.weather_forecast_model.CityWeatherForecast
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    companion object {
        const val apiKey = "8690c561fbde90f53ac2fe5b7bf7dd6f"
    }

    @GET("weather")
    fun getCurrentWeather(
        @Query("q") name: String,
        @Query("appid") appid: String = apiKey
    ): Single<CityCurrentWeather>

    @GET("forecast")
    fun getWeatherForecast(
        @Query("q") name: String,
        @Query("appid") appid: String = apiKey,
        @Query("cnt") cnt: Int = 30,
    ): Single<CityWeatherForecast>

    @GET("find")
    fun getCitiesList(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("cnt") cnt: Int = 10,
        @Query("appid") appId: String = apiKey
    ): Single<Response>
}