package com.bignerdranch.android.digest.data.current_weather

import com.bignerdranch.android.digest.domain.weather.WeatherRepository
import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.current_weather_model.Response
import com.bignerdranch.android.digest.domain.weather.weather_forecast_model.CityWeatherForecast
import io.reactivex.Single

class WeatherRepositoryImpl(private val weatherDataSource: WeatherDataSource): WeatherRepository {
    override fun getCurrentWeather(cityName: String): Single<CityCurrentWeather> = weatherDataSource.getCurrentWeather(cityName)
    override fun getWeatherForecast(cityName: String): Single<CityWeatherForecast> = weatherDataSource.getWeatherForecast(cityName)

    override fun getCitiesList(lat: Double, lon: Double): Single<Response> = weatherDataSource.getCitiesList(lat, lon)
}