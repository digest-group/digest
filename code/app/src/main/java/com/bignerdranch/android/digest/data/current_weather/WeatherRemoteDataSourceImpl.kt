package com.bignerdranch.android.digest.data.current_weather

import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.current_weather_model.Response
import com.bignerdranch.android.digest.domain.weather.weather_forecast_model.CityWeatherForecast
import io.reactivex.Single


class WeatherRemoteDataSourceImpl(private val cityApi: WeatherApi): WeatherDataSource {
    override fun getCurrentWeather(cityName: String): Single<CityCurrentWeather> = cityApi.getCurrentWeather(cityName)
    override fun getWeatherForecast(cityName: String): Single<CityWeatherForecast> = cityApi.getWeatherForecast(cityName)

    override fun getCitiesList(lat: Double, lon: Double): Single<Response> = cityApi.getCitiesList(lat, lon)

}