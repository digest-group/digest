package com.bignerdranch.android.digest.domain.news.favorite_news_usecase

import com.bignerdranch.android.digest.domain.news.FavoriteNewsRepository
import com.bignerdranch.android.digest.domain.news.news_model.Article
import io.reactivex.Completable

class InsertFavoriteNewsUseCase(private val favoriteNewsRepository: FavoriteNewsRepository) {
    operator fun invoke(favoriteArticle: Article): Completable =
        favoriteNewsRepository.insertFavoriteNews(favoriteArticle)
}