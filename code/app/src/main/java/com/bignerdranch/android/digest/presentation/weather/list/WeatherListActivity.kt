package com.bignerdranch.android.digest.presentation.weather.list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.bignerdranch.android.digest.databinding.ActivityWeatherListBinding
import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.presentation.OptionsActivity
import com.bignerdranch.android.digest.presentation.adapters.weather.CurrentAdapter
import com.bignerdranch.android.digest.presentation.weather.details.WeatherDetailsActivity
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter
import javax.inject.Inject
import javax.inject.Provider


@AndroidEntryPoint
class WeatherListActivity : MvpAppCompatActivity(), WeatherListView {

    @Inject
    lateinit var presenterProvider: Provider<WeatherListPresenter>
    private val weatherListPresenter by moxyPresenter { presenterProvider.get() }

    private val adapter = CurrentAdapter {
        weatherListPresenter.onCityClicked(it)
    }

    private lateinit var activityWeatherListBinding: ActivityWeatherListBinding

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, WeatherListActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setContentView(activityWeatherListBinding.root)
    }

    private fun initViews() {
        activityWeatherListBinding = ActivityWeatherListBinding.inflate(layoutInflater).apply {
            findButton.setOnClickListener {
                val cityName = activityWeatherListBinding.cityEditText.text.toString()
                weatherListPresenter.openDetails(cityName)
            }

            weatherTabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    if (tab?.position == 0) {
                        findButton.isEnabled = true
                        weatherListPresenter.onCurrentClicked(this@WeatherListActivity)
                    } else {
                        findButton.isEnabled = false
                        weatherListPresenter.onFavoritesClicked()
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {}
                override fun onTabReselected(tab: TabLayout.Tab?) {}
            })

            citiesList.adapter = adapter
            citiesList.layoutManager = LinearLayoutManager(this@WeatherListActivity)
            backButtonWeatherList.setOnClickListener {
                OptionsActivity.start(this@WeatherListActivity)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        val selectedTabPosition = activityWeatherListBinding.weatherTabs.selectedTabPosition
        if (selectedTabPosition == 0) {
            weatherListPresenter.onCurrentClicked(this)
        } else {
            weatherListPresenter.onFavoritesClicked()
        }
    }

    override fun cleanCitiesList() {
        adapter.cityCurrents = emptyList()
    }

    override fun setIsLoading(isLoading: Boolean) {
        activityWeatherListBinding.apply {
            mainProgressBar.isVisible = isLoading
            citiesList.isVisible = !isLoading
        }
    }

    override fun bindCitiesList(cityCurrents: List<CityCurrentWeather>) {
        adapter.cityCurrents = cityCurrents
    }

    override fun bindOneCity(city: CityCurrentWeather) {
        val newList: MutableList<CityCurrentWeather> = mutableListOf()
        newList.addAll(adapter.cityCurrents)
        newList.add(city)
        adapter.cityCurrents = newList.toList()
    }

    override fun openDetails(cityName: String) {
        WeatherDetailsActivity.start(this, cityName)
    }

    override fun showError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }
}