package com.bignerdranch.android.digest.presentation.weather.list

import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleTagStrategy::class)
interface WeatherListView: MvpView {

    fun setIsLoading(isLoading: Boolean)
    fun openDetails(cityName: String)
    fun bindCitiesList(cityCurrents: List<CityCurrentWeather>)
    fun cleanCitiesList()
    fun bindOneCity(city: CityCurrentWeather)
    fun showError(message: String)

}