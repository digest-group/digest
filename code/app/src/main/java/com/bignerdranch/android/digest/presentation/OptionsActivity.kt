package com.bignerdranch.android.digest.presentation

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bignerdranch.android.digest.R
import com.bignerdranch.android.digest.databinding.ActivityOptionsBinding
import com.bignerdranch.android.digest.presentation.account.AccountActivity
import com.bignerdranch.android.digest.presentation.news.list.NewsListActivity
import com.bignerdranch.android.digest.presentation.weather.list.WeatherListActivity

class OptionsActivity : AppCompatActivity() {

    companion object {
        const val CHANNEL_ID = "digestChannel"
        fun start(context: Context) {
            val intent = Intent(context, OptionsActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var activityOptionsBinding: ActivityOptionsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createNotificationChannels()
        activityOptionsBinding = ActivityOptionsBinding.inflate(layoutInflater)
        setContentView(activityOptionsBinding.root)
        activityOptionsBinding.weatherButton.setOnClickListener {
            WeatherListActivity.start(this)
        }
        activityOptionsBinding.newsButton.setOnClickListener {
            NewsListActivity.start(this)
        }
        activityOptionsBinding.accountButton.setOnClickListener {
            AccountActivity.start(this)
        }
    }

    private fun createNotificationChannels(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.notificationChannel)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

}