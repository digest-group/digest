package com.bignerdranch.android.digest.domain.news.favorite_news_usecase

import com.bignerdranch.android.digest.domain.news.FavoriteNewsRepository
import com.bignerdranch.android.digest.domain.news.news_model.Article
import com.bignerdranch.android.digest.domain.news.news_model.BreakingNews
import io.reactivex.Single

class GetAllFavoriteNewsUseCase(private val favoriteNewsRepository: FavoriteNewsRepository) {
    operator fun invoke(): Single<List<Article>> = favoriteNewsRepository.getAllFavoriteNews()
}