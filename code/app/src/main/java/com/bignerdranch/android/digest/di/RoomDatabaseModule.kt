package com.bignerdranch.android.digest.di

import android.content.Context
import androidx.room.Room
import com.bignerdranch.android.digest.data.favorites.FavoritesDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomDatabaseModule {
    @Singleton
    @Provides
    fun providesDatabase(@ApplicationContext applicationContext: Context) =
        Room.databaseBuilder(
            applicationContext,
            FavoritesDatabase::class.java, "favorite_database"
        )
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun providesFavoriteCityDao(database: FavoritesDatabase) = database.getFavoriteCityDao()

    @Singleton
    @Provides
    fun providesFavoriteNewsDao(database: FavoritesDatabase) = database.getFavoriteNewsDao()
}