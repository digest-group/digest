package com.bignerdranch.android.digest.domain.weather.weather_forecast_model

data class Weather(
    val icon: String,
    val id: Int
)