package com.bignerdranch.android.digest.presentation.account

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class AccountPresenter: MvpPresenter<AccountView>() {

    fun logout(firebaseAuth: FirebaseAuth) {
        firebaseAuth.signOut()
        viewState.openLoginActivity()
    }

    fun loadUserInfo(firebaseAuth: FirebaseAuth, firebaseFirestore: FirebaseFirestore) {
        val user = firebaseAuth.currentUser
        val email = user?.email ?: "unknown"
        firebaseFirestore.collection("users").document(email).get()
            .addOnSuccessListener {
                val name = it.getString("name") ?: "unknown"
                val gender = it.getString("gender") ?: "unknown"
                val userData = User(name = name, gender = gender, email = email)
                viewState.bindUser(userData)
            }

    }

}