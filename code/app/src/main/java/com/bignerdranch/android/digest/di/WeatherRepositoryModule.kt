package com.bignerdranch.android.digest.di

import com.bignerdranch.android.digest.data.current_weather.WeatherDataSource
import com.bignerdranch.android.digest.data.current_weather.WeatherRemoteDataSourceImpl
import com.bignerdranch.android.digest.data.current_weather.WeatherRepositoryImpl
import com.bignerdranch.android.digest.data.current_weather.WeatherApi
import com.bignerdranch.android.digest.domain.weather.WeatherRepository
import com.bignerdranch.android.digest.domain.weather.weather_usecase.GetCitiesListUseCase
import com.bignerdranch.android.digest.domain.weather.weather_usecase.GetCurrentWeatherUseCase
import com.bignerdranch.android.digest.domain.weather.weather_usecase.GetWeatherForecastUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class WeatherRepositoryModule {

    @Singleton
    @Provides
    fun providesCityRemoteDataSource(weatherApi: WeatherApi): WeatherDataSource =
            WeatherRemoteDataSourceImpl(weatherApi)

    @Singleton
    @Provides
    fun providesCityRepository(weatherDataSource: WeatherDataSource): WeatherRepository =
            WeatherRepositoryImpl(weatherDataSource)

    @Singleton
    @Provides
    fun providesGetCurrentWeatherUseCase(weatherRepository: WeatherRepository): GetCurrentWeatherUseCase =
            GetCurrentWeatherUseCase(weatherRepository)

    @Singleton
    @Provides
    fun providesGetCitiesListUseCase(weatherRepository: WeatherRepository): GetCitiesListUseCase =
        GetCitiesListUseCase(weatherRepository)

    @Singleton
    @Provides
    fun providesGetWeatherForecastUseCase(weatherRepository: WeatherRepository): GetWeatherForecastUseCase =
        GetWeatherForecastUseCase(weatherRepository)
}