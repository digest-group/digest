package com.bignerdranch.android.digest.data.favorites.news

import androidx.room.*
import com.bignerdranch.android.digest.domain.news.news_model.Article
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface FavoriteNewsDao {

    @Query("SELECT * FROM Article")
    fun getAllFavoriteArticles(): Single<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticle(favoriteArticle: Article): Completable

    @Delete
    fun deleteFavoriteArticle(favoriteArticle: Article): Completable

    @Query("SELECT * FROM Article WHERE title = :title")
    fun getFavoriteArticle(title: String): Single<Article>

}