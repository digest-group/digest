package com.bignerdranch.android.digest.domain.weather.favorite_city_usecase

import com.bignerdranch.android.digest.domain.weather.FavoriteCityRepository
import io.reactivex.Completable

class DeleteFavoriteCityUseCase(private val favoriteCityRepository: FavoriteCityRepository) {
    operator fun invoke(favoriteCityName: String): Completable = favoriteCityRepository.deleteFavoriteCity(favoriteCityName)
}