package com.bignerdranch.android.digest.presentation.weather.details

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_CANCEL_CURRENT
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.DEFAULT_SOUND
import androidx.core.app.NotificationCompat.DEFAULT_VIBRATE
import androidx.core.app.NotificationManagerCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.bignerdranch.android.digest.R
import com.bignerdranch.android.digest.databinding.ActivityWeatherDetailsBinding
import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.weather_forecast_model.Forecast
import com.bignerdranch.android.digest.presentation.OptionsActivity.Companion.CHANNEL_ID
import com.bignerdranch.android.digest.presentation.adapters.weather.ForecastAdapter
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

@AndroidEntryPoint
class WeatherDetailsActivity : MvpAppCompatActivity(), WeatherDetailsView {

    @Inject
    lateinit var presenterProvider: Provider<WeatherDetailsPresenter>
    private val weatherDetailsPresenter by moxyPresenter { presenterProvider.get() }
    private lateinit var cityName: String

    private lateinit var activityWeatherDetailsBinding: ActivityWeatherDetailsBinding
    private val adapter = ForecastAdapter()
    private lateinit var builder: NotificationCompat.Builder
    private var notificationId = 0

    companion object {
        private const val EXTRA_NAME = "EXTRA_NAME"

        fun start(context: Context, cityName: String) {
            val intent = Intent(context, WeatherDetailsActivity::class.java)
            intent.putExtra(EXTRA_NAME, cityName)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cityName = intent.getStringExtra(EXTRA_NAME)?.toLowerCase(Locale.ROOT) ?: "Unknown"
        initViews()
        setContentView(activityWeatherDetailsBinding.root)
        weatherDetailsPresenter.loadCity(cityName)
        weatherDetailsPresenter.loadForecast(cityName)
        createNotificationBuilder()
        weatherDetailsPresenter.createTimer()
    }

    private fun createNotificationBuilder(){
        val intent = Intent(this, WeatherDetailsActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(EXTRA_NAME, cityName)
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, FLAG_CANCEL_CURRENT)
        val prettyCityName = cityName.capitalize(Locale.ROOT)
        builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_weather)
            .setContentTitle("The weather has changed")
            .setContentText("The weather in $prettyCityName has changed! Please update your screen.")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(DEFAULT_SOUND or DEFAULT_VIBRATE)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
    }

    override fun notifyAboutWeather(){
        with(NotificationManagerCompat.from(this)) {
            notify(++notificationId, builder.build())
        }
    }

    private fun initViews() {
        activityWeatherDetailsBinding = ActivityWeatherDetailsBinding.inflate(layoutInflater)
        activityWeatherDetailsBinding.backButton.setOnClickListener {
            weatherDetailsPresenter.goBack()
        }
        weatherDetailsPresenter.haveInFavorites(cityName)
        activityWeatherDetailsBinding.addToFavoriteButton.setOnCheckedChangeListener { _, isChecked ->
            weatherDetailsPresenter.onAddToFavoriteButtonClicked(cityName, !isChecked)
        }
    }

    override fun setIsCurrentLoading(isLoading: Boolean) {
        activityWeatherDetailsBinding.detailsProgressBar.isVisible = isLoading
        activityWeatherDetailsBinding.detailsLayout.isVisible = !isLoading
    }

    override fun setIsInFavorites(isFavorite: Boolean) {
        activityWeatherDetailsBinding.addToFavoriteButton.isChecked = isFavorite
    }

    override fun setIsForecastLoading(isLoading: Boolean) {
        activityWeatherDetailsBinding.forecastProgressBar.isVisible = isLoading
        activityWeatherDetailsBinding.forecastList.isVisible = !isLoading
    }

    override fun bindCity(cityCurrentWeather: CityCurrentWeather) {
        activityWeatherDetailsBinding.countryText.text =
            applicationContext.getString(
                R.string.country_format, cityCurrentWeather.sys.country
            )
        activityWeatherDetailsBinding.cityNameText.text = cityCurrentWeather.name
        activityWeatherDetailsBinding.tempText.text = applicationContext.getString(
            R.string.temp_format,
            weatherDetailsPresenter.convertTemp(cityCurrentWeather.main.temp).toString()
        )
        activityWeatherDetailsBinding.feelsLikeText.text = applicationContext.getString(
            R.string.feels_like_format,
            weatherDetailsPresenter.convertTemp(cityCurrentWeather.main.feels_like).toString()
        )
        activityWeatherDetailsBinding.humidText.text = applicationContext.getString(
            R.string.humid_format,
            cityCurrentWeather.main.humidity.toString()
        )
        activityWeatherDetailsBinding.condText.text = applicationContext.getString(
            R.string.cond_format,
            cityCurrentWeather.weather[0].description
        )
        weatherDetailsPresenter.loadWeatherPicture(
            applicationContext,
            cityCurrentWeather,
            activityWeatherDetailsBinding.weatherImage
        )

        activityWeatherDetailsBinding.forecastList.adapter = adapter
        activityWeatherDetailsBinding.forecastList.layoutManager = LinearLayoutManager(this)
    }

    override fun bindForecastList(forecastList: List<Forecast>) {
        adapter.cityForecast = forecastList
    }

    override fun goBack() {
        finish()
    }
}