package com.bignerdranch.android.digest.domain.weather

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavoriteCity(
    @PrimaryKey
    @ColumnInfo(name = "name")
    val name: String
)