package com.bignerdranch.android.digest.domain.weather.weather_usecase

import com.bignerdranch.android.digest.domain.weather.WeatherRepository

class GetWeatherForecastUseCase(private val weatherRepository: WeatherRepository) {
    operator fun invoke(cityName: String) = weatherRepository.getWeatherForecast(cityName)
}