package com.bignerdranch.android.digest.presentation.login

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleTagStrategy::class)
interface MainView: MvpView {
    fun openOptionsActivity()
    fun openSignUpActivity()
    fun showError(message: String)
}