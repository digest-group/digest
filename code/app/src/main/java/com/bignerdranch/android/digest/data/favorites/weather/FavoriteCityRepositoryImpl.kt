package com.bignerdranch.android.digest.data.favorites.weather

import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import com.bignerdranch.android.digest.domain.weather.FavoriteCityRepository
import io.reactivex.Completable
import io.reactivex.Single

class FavoriteCityRepositoryImpl(private val favoriteCityDataSource: FavoriteCityDataSource) :
    FavoriteCityRepository {
    override fun getAllFavoritesCities(): Single<List<FavoriteCity>> =
        favoriteCityDataSource.getAllFavoritesCities()

    override fun insertFavoriteCity(favoriteCity: FavoriteCity): Completable =
        favoriteCityDataSource.insertFavoriteCity(favoriteCity)

    override fun deleteFavoriteCity(favoriteCityName: String): Completable =
        favoriteCityDataSource.deleteFavoriteCity(favoriteCityName)

    override fun getFavoriteCity(favoriteCityName: String): Single<FavoriteCity> =
        favoriteCityDataSource.getCity(favoriteCityName)
}