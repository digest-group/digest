package com.bignerdranch.android.digest.presentation.news.list

import android.util.Log
import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.GetAllFavoriteNewsUseCase
import com.bignerdranch.android.digest.domain.news.news_model.Article
import com.bignerdranch.android.digest.domain.news.news_usecase.GetBreakingNewsUseCase
import com.bignerdranch.android.digest.domain.news.news_usecase.GetEverythingUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.timer

@InjectViewState
class NewsListPresenter @Inject constructor(
        private val getBreakingNewsUseCase: GetBreakingNewsUseCase,
        private val getEverythingUseCase: GetEverythingUseCase,
        private val getAllFavoriteNewsUseCase: GetAllFavoriteNewsUseCase
) : MvpPresenter<NewsListView>() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private fun Disposable.untilDestroy() {
        compositeDisposable.add(this)
    }

    private lateinit var timerForCash: Timer
    private var currentArticles: List<Article>? = null

    fun createTimer() {
        timerForCash = timer(
                initialDelay = 30000,
                period = 30000,
                action = {
                    currentArticles?.let {
                        reloadBreakingNews()
                    }
                }
        )
        Log.i("#@!", "Timer was created!")
    }

    private fun reloadBreakingNews() {
        Log.i("#@!", "Reload!")
        getBreakingNewsUseCase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (newsHaveChanged(it.list)) {
                        Log.i("#@!", "News have changed!")
                        viewState.notifyAboutNews()
                    } else {
                        Log.i("#@!", "News have not changed!")
                    }
                }, {})
                .untilDestroy()
    }

    private fun newsHaveChanged(articles: List<Article>): Boolean {
        articles.forEach {
            if (!currentArticles!!.contains(it)) {
                currentArticles = articles
                return true
            }
        }
        return false
    }

    fun onFindButtonClicked(query: String) {
        viewState.setIsLoading(true)
        CountingIdlingResourceSingleton.increment()
        getEverythingUseCase(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate {
                viewState.setIsLoading(false)
                CountingIdlingResourceSingleton.decrement()
            }
            .subscribe({
                viewState.bindNewsList(it.list)
            }, {
                it.printStackTrace()
            })
            .untilDestroy()
    }

    fun onFavoritesTabClicked() {
        viewState.setIsLoading(true)
        getAllFavoriteNewsUseCase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate {
                    viewState.setIsLoading(false)
                }
                .subscribe({
                    viewState.bindNewsList(it)
                }, {
                    it.printStackTrace()
                })
                .untilDestroy()
    }

    fun onNewsTabClicked() {
        viewState.setIsLoading(true)
        CountingIdlingResourceSingleton.increment()
        getBreakingNewsUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate {
                viewState.setIsLoading(false)
                CountingIdlingResourceSingleton.decrement()
            }
            .subscribe({
                viewState.bindNewsList(it.list)
            }, {
                it.printStackTrace()
            })
            .untilDestroy()
    }

    fun onNewsClicked(article: Article) {
        viewState.openNewsDetails(article)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
        timerForCash.cancel()
    }

}