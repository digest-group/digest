package com.bignerdranch.android.digest.domain.weather.current_weather_model

data class CityCurrentWeather(
        val main: Main,
        val name: String,
        val sys: Sys,
        val weather: List<Weather>
)

