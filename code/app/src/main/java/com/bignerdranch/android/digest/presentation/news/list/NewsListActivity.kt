package com.bignerdranch.android.digest.presentation.news.list

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.bignerdranch.android.digest.R
import com.bignerdranch.android.digest.databinding.ActivityNewsListBinding
import com.bignerdranch.android.digest.domain.news.news_model.Article
import com.bignerdranch.android.digest.presentation.OptionsActivity
import com.bignerdranch.android.digest.presentation.adapters.news.NewsAdapter
import com.bignerdranch.android.digest.presentation.news.details.NewsDetailsActivity
import com.bignerdranch.android.digest.presentation.weather.details.WeatherDetailsActivity
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

@AndroidEntryPoint
class NewsListActivity : MvpAppCompatActivity(), NewsListView {

    @Inject
    lateinit var presenterProvider: Provider<NewsListPresenter>
    private val newsListPresenter by moxyPresenter { presenterProvider.get() }

    private lateinit var activityNewsListBinding: ActivityNewsListBinding
    private lateinit var builder: NotificationCompat.Builder
    private var notificationId = 0

    private val adapter = NewsAdapter {
        newsListPresenter.onNewsClicked(it)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, NewsListActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setContentView(activityNewsListBinding.root)
        createNotificationBuilder()
        newsListPresenter.createTimer()
    }

    private fun initViews() {
        activityNewsListBinding = ActivityNewsListBinding.inflate(layoutInflater).apply {
            newsTabs.addOnTabSelectedListener(object :
                    TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    if (tab?.position == 0) {
                        findButton.isEnabled = true
                        newsListPresenter.onNewsTabClicked()
                    } else {
                        findButton.isEnabled = false
                        newsListPresenter.onFavoritesTabClicked()
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {}
                override fun onTabReselected(tab: TabLayout.Tab?) {}
            })
            newsList.adapter = adapter
            newsList.layoutManager = LinearLayoutManager(this@NewsListActivity)
            backButtonNewsList.setOnClickListener {
                OptionsActivity.start(this@NewsListActivity)
            }
            findButton.setOnClickListener {
                val query = newsEditText.text.toString()
                newsListPresenter.onFindButtonClicked(query)
            }
        }

    }

    private fun createNotificationBuilder(){
        val intent = Intent(this, NewsListActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        )
        builder = NotificationCompat.Builder(this, OptionsActivity.CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_news)
                .setContentTitle("Breaking news have changed!")
                .setContentText("Breaking news have changed! Please, update your screen.")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_SOUND or NotificationCompat.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
    }

    override fun notifyAboutNews() {
        with(NotificationManagerCompat.from(this)) {
            notify(++notificationId, builder.build())
        }
    }

    override fun openNewsDetails(article: Article) {
        NewsDetailsActivity.start(this, article)
    }

    override fun onResume() {
        super.onResume()
        val selectedTabPosition = activityNewsListBinding.newsTabs.selectedTabPosition
        if (selectedTabPosition == 0) {
            newsListPresenter.onNewsTabClicked()
        }
    }

    override fun setIsLoading(isLoading: Boolean) {
        activityNewsListBinding.apply {
            mainProgressBar.isVisible = isLoading
            newsList.isVisible = !isLoading
        }
    }

    override fun bindNewsList(news: List<Article>) {
        adapter.breakingNews = news
    }
}