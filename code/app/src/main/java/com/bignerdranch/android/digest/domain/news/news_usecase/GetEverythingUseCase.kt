package com.bignerdranch.android.digest.domain.news.news_usecase

import com.bignerdranch.android.digest.domain.news.NewsRepository
import com.bignerdranch.android.digest.domain.news.news_model.BreakingNews
import io.reactivex.Single

class GetEverythingUseCase(private val newsRepository: NewsRepository) {
    operator fun invoke(query: String): Single<BreakingNews> = newsRepository.getEverything(query)
}