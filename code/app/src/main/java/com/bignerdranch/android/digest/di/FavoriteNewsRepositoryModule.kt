package com.bignerdranch.android.digest.di

import com.bignerdranch.android.digest.data.favorites.news.FavoriteNewsDao
import com.bignerdranch.android.digest.data.favorites.news.FavoriteNewsDataSource
import com.bignerdranch.android.digest.data.favorites.news.FavoriteNewsLocalDataSource
import com.bignerdranch.android.digest.data.favorites.news.FavoriteNewsRepositoryImpl
import com.bignerdranch.android.digest.domain.news.FavoriteNewsRepository
import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.DeleteFavoriteNewsUseCase
import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.GetAllFavoriteNewsUseCase
import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.GetFavoriteNewsUseCase
import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.InsertFavoriteNewsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class FavoriteNewsRepositoryModule {

    @Singleton
    @Provides
    fun provideFavoritesDataSource(favoriteNewsDao: FavoriteNewsDao): FavoriteNewsDataSource =
        FavoriteNewsLocalDataSource(favoriteNewsDao)

    @Singleton
    @Provides
    fun provideFavoriteNewsRepository(favoriteNewsDataSource: FavoriteNewsDataSource): FavoriteNewsRepository =
        FavoriteNewsRepositoryImpl(favoriteNewsDataSource)

    @Singleton
    @Provides
    fun provideGetAllFavoriteNewsUseCase(favoriteNewsRepository: FavoriteNewsRepository): GetAllFavoriteNewsUseCase =
        GetAllFavoriteNewsUseCase(favoriteNewsRepository)

    @Singleton
    @Provides
    fun provideInsertFavoriteNewsUseCase(favoriteNewsRepository: FavoriteNewsRepository): InsertFavoriteNewsUseCase =
        InsertFavoriteNewsUseCase(favoriteNewsRepository)

    @Singleton
    @Provides
    fun provideDeleteFavoriteNewsUseCase(favoriteNewsRepository: FavoriteNewsRepository): DeleteFavoriteNewsUseCase =
        DeleteFavoriteNewsUseCase(favoriteNewsRepository)

    @Singleton
    @Provides
    fun provideGetFavoriteNewsUseCase(favoriteNewsRepository: FavoriteNewsRepository): GetFavoriteNewsUseCase =
        GetFavoriteNewsUseCase(favoriteNewsRepository)
}