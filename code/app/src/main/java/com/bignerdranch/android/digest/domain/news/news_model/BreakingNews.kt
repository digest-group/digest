package com.bignerdranch.android.digest.domain.news.news_model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BreakingNews(
    @SerializedName("articles")
    val list: List<Article>
) : Serializable