package com.bignerdranch.android.digest.data.favorites.weather

import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import io.reactivex.Completable
import io.reactivex.Single

interface FavoriteCityDataSource {
    fun getAllFavoritesCities(): Single<List<FavoriteCity>>
    fun insertFavoriteCity(favoriteCity: FavoriteCity): Completable
    fun deleteFavoriteCity(favoriteCityName: String): Completable
    fun getCity(favoriteCityName: String): Single<FavoriteCity>
}