package com.bignerdranch.android.digest.domain.news

import com.bignerdranch.android.digest.domain.news.news_model.BreakingNews
import io.reactivex.Single

interface NewsRepository {
    fun getBreakingNews(): Single<BreakingNews>
    fun getEverything(query: String): Single<BreakingNews>
}