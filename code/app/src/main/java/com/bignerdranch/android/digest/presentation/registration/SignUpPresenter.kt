package com.bignerdranch.android.digest.presentation.registration

import android.util.Log
import com.bignerdranch.android.digest.databinding.ActivitySignUpBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class SignUpPresenter : MvpPresenter<SignUpView>() {

    fun onEndSignUpButtonClicked(
        signUpBinding: ActivitySignUpBinding,
        firebaseAuth: FirebaseAuth,
        firebaseFirestore: FirebaseFirestore
    ) {
        val email = signUpBinding.emailEditText.text.toString()
        val password = signUpBinding.passwordEditText.text.toString()
        val name = signUpBinding.nameEditText.text.toString()
        val gender = if (signUpBinding.maleButton.isChecked) {
            signUpBinding.maleButton.text.toString()
        } else {
            signUpBinding.femaleButton.text.toString()
        }
        if (!validateFields(email, password, name)) {
            viewState.showError("Please enter all fields!")
            return
        }
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    storeData(email, name, gender, firebaseFirestore)
                    viewState.openLoginActivity()
                } else {
                    task.exception?.message?.let { viewState.showError(it) }
                    task.exception?.printStackTrace()
                }
            }
    }

    private fun storeData(email: String, name: String, gender: String, firebaseFirestore: FirebaseFirestore){
        val user = hashMapOf(
            "name" to name,
            "gender" to gender
        )
        firebaseFirestore.collection("users").document(email)
            .set(user)
            .addOnFailureListener {
                it.printStackTrace()
            }
            .addOnSuccessListener {
                Log.i(null, "Registration is successful")
            }
    }

    private fun validateFields(email: String, password: String, name: String): Boolean {

        return email.isNotEmpty() && password.isNotEmpty() && name.isNotEmpty()
    }
}