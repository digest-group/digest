package com.bignerdranch.android.digest.domain.news.favorite_news_usecase

import com.bignerdranch.android.digest.domain.news.FavoriteNewsRepository
import com.bignerdranch.android.digest.domain.news.news_model.Article
import io.reactivex.Single

class GetFavoriteNewsUseCase(private val favoriteNewsRepository: FavoriteNewsRepository) {
    operator fun invoke(title: String): Single<Article> =
        favoriteNewsRepository.getFavoriteNews(title)
}