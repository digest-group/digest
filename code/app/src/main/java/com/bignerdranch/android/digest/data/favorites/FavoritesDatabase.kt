package com.bignerdranch.android.digest.data.favorites

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bignerdranch.android.digest.data.favorites.news.FavoriteNewsDao
import com.bignerdranch.android.digest.data.favorites.weather.FavoriteCityDao
import com.bignerdranch.android.digest.domain.news.news_model.Article
import com.bignerdranch.android.digest.domain.weather.FavoriteCity

@Database(entities = [FavoriteCity::class, Article::class], version = 3)
abstract class FavoritesDatabase : RoomDatabase(){
    abstract fun getFavoriteCityDao(): FavoriteCityDao
    abstract fun getFavoriteNewsDao(): FavoriteNewsDao
}