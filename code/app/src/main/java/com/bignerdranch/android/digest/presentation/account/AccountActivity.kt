package com.bignerdranch.android.digest.presentation.account

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.bignerdranch.android.digest.databinding.ActivityAccountBinding
import com.bignerdranch.android.digest.presentation.login.MainActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class AccountActivity : MvpAppCompatActivity(), AccountView {

    @InjectPresenter
    lateinit var presenter: AccountPresenter
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var firebaseFirestore: FirebaseFirestore

    private lateinit var binding: ActivityAccountBinding

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, AccountActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setContentView(binding.root)
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseFirestore = FirebaseFirestore.getInstance()
        presenter.loadUserInfo(firebaseAuth, firebaseFirestore)
    }

    private fun initViews() {
        binding = ActivityAccountBinding.inflate(layoutInflater)
        binding.logoutButton.setOnClickListener {
            presenter.logout(firebaseAuth)
        }
    }

    override fun bindUser(user: User) {
        binding.emailText.text = user.email
        binding.genderText.text = user.gender
        binding.nameText.text = user.name
    }

    override fun openLoginActivity() {
        MainActivity.start(this)
    }

}