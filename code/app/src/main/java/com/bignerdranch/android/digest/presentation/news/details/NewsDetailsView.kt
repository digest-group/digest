package com.bignerdranch.android.digest.presentation.news.details

import com.bignerdranch.android.digest.domain.news.news_model.Article
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleTagStrategy::class)
interface NewsDetailsView: MvpView {

    fun bindArticle(article: Article)
    fun openNewsList()
    fun setIsInFavorites(isInFavorites: Boolean)
}