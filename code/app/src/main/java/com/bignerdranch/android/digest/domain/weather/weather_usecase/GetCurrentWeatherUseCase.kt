package com.bignerdranch.android.digest.domain.weather.weather_usecase

import com.bignerdranch.android.digest.domain.weather.WeatherRepository
import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import io.reactivex.Single

class GetCurrentWeatherUseCase(private val weatherRepository: WeatherRepository) {
    operator fun invoke(cityName: String): Single<CityCurrentWeather> =
        weatherRepository.getCurrentWeather(cityName)
}