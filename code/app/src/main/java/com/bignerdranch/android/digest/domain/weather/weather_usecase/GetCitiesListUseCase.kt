package com.bignerdranch.android.digest.domain.weather.weather_usecase

import com.bignerdranch.android.digest.domain.weather.WeatherRepository
import com.bignerdranch.android.digest.domain.weather.current_weather_model.Response
import io.reactivex.Single

class GetCitiesListUseCase(private val weatherRepository: WeatherRepository) {
    operator fun invoke(lat: Double, lon: Double): Single<Response> = weatherRepository.getCitiesList(lat, lon)
}