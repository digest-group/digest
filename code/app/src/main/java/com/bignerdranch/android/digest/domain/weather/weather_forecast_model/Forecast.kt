package com.bignerdranch.android.digest.domain.weather.weather_forecast_model

import com.google.gson.annotations.SerializedName

data class Forecast(
    val dt: Long,
    @SerializedName("dt_txt")
    val dtTxt: String,
    val main: Main,
    val weather: List<Weather>
)