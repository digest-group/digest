package com.bignerdranch.android.digest.di

import com.bignerdranch.android.digest.data.current_weather.WeatherApi
import com.bignerdranch.android.digest.data.news.NewsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RetrofitModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class WeatherApiProvider

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class NewsApiProvider

    @WeatherApiProvider
    @Singleton
    @Provides
    fun provideRetrofitForWeather(): Retrofit =
            Retrofit.Builder()
                    .client(OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().also {
                        it.level = HttpLoggingInterceptor.Level.BODY
                    }).build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://api.openweathermap.org/data/2.5/")
                    .build()

    @NewsApiProvider
    @Singleton
    @Provides
    fun provideRetrofitForNews(): Retrofit =
        Retrofit.Builder()
            .client(OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().also {
                it.level = HttpLoggingInterceptor.Level.BODY
            }).build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://newsapi.org/v2/")
            .build()

    @Singleton
    @Provides
    fun providesWeatherApi(@WeatherApiProvider retrofit: Retrofit): WeatherApi =
            retrofit.create(WeatherApi::class.java)


    @Singleton
    @Provides
    fun providesNewsApi(@NewsApiProvider retrofit: Retrofit): NewsApi =
        retrofit.create(NewsApi::class.java)

}