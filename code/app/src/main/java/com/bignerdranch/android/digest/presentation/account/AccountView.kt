package com.bignerdranch.android.digest.presentation.account

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleTagStrategy::class)
interface AccountView: MvpView {

    fun openLoginActivity()
    fun bindUser(user: User)
}