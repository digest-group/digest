package com.bignerdranch.android.digest.data.news

import com.bignerdranch.android.digest.domain.news.news_model.BreakingNews
import io.reactivex.Single

interface NewsDataSource {
    fun getBreakingNews(): Single<BreakingNews>
    fun getEverything(query: String) : Single<BreakingNews>
}