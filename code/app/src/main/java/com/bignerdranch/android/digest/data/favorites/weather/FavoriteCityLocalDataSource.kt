package com.bignerdranch.android.digest.data.favorites.weather

import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import io.reactivex.Completable
import io.reactivex.Single

class FavoriteCityLocalDataSource(private val favoriteCityDao: FavoriteCityDao) :
    FavoriteCityDataSource {
    override fun getAllFavoritesCities(): Single<List<FavoriteCity>> = favoriteCityDao.getAll()

    override fun insertFavoriteCity(favoriteCity: FavoriteCity): Completable =
        favoriteCityDao.insertCity(favoriteCity)

    override fun deleteFavoriteCity(favoriteCityName: String): Completable =
        favoriteCityDao.deleteCity(favoriteCityName)

    override fun getCity(favoriteCityName: String): Single<FavoriteCity> =
        favoriteCityDao.getCity(favoriteCityName)
}