package com.bignerdranch.android.digest.presentation.weather.details

import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.weather_forecast_model.Forecast
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleTagStrategy::class)
interface WeatherDetailsView: MvpView {

    fun setIsCurrentLoading(isLoading: Boolean)
    fun setIsForecastLoading(isLoading: Boolean)
    fun setIsInFavorites(isFavorite: Boolean)
    fun bindCity(cityCurrentWeather: CityCurrentWeather)
    fun bindForecastList(forecastList: List<Forecast>)
    fun goBack()
    fun notifyAboutWeather()


}