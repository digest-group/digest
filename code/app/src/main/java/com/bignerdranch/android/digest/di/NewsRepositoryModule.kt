package com.bignerdranch.android.digest.di

import com.bignerdranch.android.digest.data.news.NewsApi
import com.bignerdranch.android.digest.data.news.NewsDataSource
import com.bignerdranch.android.digest.data.news.NewsRemoteDataSourceImpl
import com.bignerdranch.android.digest.data.news.NewsRepositoryImpl
import com.bignerdranch.android.digest.domain.news.NewsRepository
import com.bignerdranch.android.digest.domain.news.news_usecase.GetBreakingNewsUseCase
import com.bignerdranch.android.digest.domain.news.news_usecase.GetEverythingUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NewsRepositoryModule {
    @Singleton
    @Provides
    fun providesNewsRemoteDataSource(newsApi: NewsApi): NewsDataSource = NewsRemoteDataSourceImpl(newsApi)

    @Singleton
    @Provides
    fun providesNewsRepository(newsDataSource: NewsDataSource): NewsRepository = NewsRepositoryImpl(newsDataSource)

    @Singleton
    @Provides
    fun providesGetBreakingNewsUseCase(newsRepository: NewsRepository): GetBreakingNewsUseCase =
        GetBreakingNewsUseCase(newsRepository)

    @Singleton
    @Provides
    fun providesGetEverythingUseCase(newsRepository: NewsRepository): GetEverythingUseCase =
        GetEverythingUseCase(newsRepository)
}