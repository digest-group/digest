package com.bignerdranch.android.digest.data.news

import com.bignerdranch.android.digest.domain.news.news_model.BreakingNews
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    companion object{
        const val apiId = "b79a57a5009f4f0d923ad35f26bbe7de"
    }

    @GET("everything")
    fun getEverything(
        @Query("q") q: String,
        @Query("language") language: String = "ru",
        @Query("apiKey") apiKey: String = apiId
    ): Single<BreakingNews>

    @GET("top-headlines")
    fun getBreakingNews(
        @Query("country") country: String = "ru",
        @Query("apiKey") apiKey: String = apiId
    ): Single<BreakingNews>
}