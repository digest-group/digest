package com.bignerdranch.android.digest.data.favorites.weather

import androidx.room.*
import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface FavoriteCityDao {
    @Query("SELECT * FROM FavoriteCity")
    fun getAll(): Single<List<FavoriteCity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCity(favoriteCity: FavoriteCity): Completable

    @Query("DELETE FROM FavoriteCity WHERE name=:favoriteCityName")
    fun deleteCity(favoriteCityName: String): Completable

    @Query("SELECT * FROM FavoriteCity WHERE name=:favoriteCityName")
    fun getCity(favoriteCityName: String): Single<FavoriteCity>
}