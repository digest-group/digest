package com.bignerdranch.android.digest.presentation.weather.details

import android.content.Context
import android.util.Log
import android.widget.ImageView
import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.DeleteFavoriteCityUseCase
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.GetFavoriteCityUseCase
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.InsertFavoriteCityUseCase
import com.bignerdranch.android.digest.domain.weather.weather_usecase.GetCurrentWeatherUseCase
import com.bignerdranch.android.digest.domain.weather.weather_usecase.GetWeatherForecastUseCase
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.timer

@InjectViewState
class WeatherDetailsPresenter @Inject constructor(
    private val getCurrentWeatherUseCase: GetCurrentWeatherUseCase,
    private val getWeatherForecastUseCase: GetWeatherForecastUseCase,
    private val insertFavoriteCityUseCase: InsertFavoriteCityUseCase,
    private val deleteFavoriteCityUseCase: DeleteFavoriteCityUseCase,
    private val getFavoriteCityUseCase: GetFavoriteCityUseCase
) : MvpPresenter<WeatherDetailsView>() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private fun Disposable.untilDestroy() {
        compositeDisposable.add(this)
    }

    private lateinit var timerForCash: Timer
    private var currentWeather: CityCurrentWeather? = null

    fun convertTemp(temp: Double): Int {
        return (temp - 273).toInt()
    }

    fun createTimer() {
        timerForCash = timer(
            initialDelay = 30000,
            period = 30000,
            action = {
                currentWeather?.let { reloadCity(it.name) }
            }
        )
    }

    private fun reloadCity(cityName: String) {
        getCurrentWeatherUseCase(cityName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (weatherHasChanged(it)) {
                    Log.i("#@!", "Weather has changed!")
                    viewState.notifyAboutWeather()
                    currentWeather = it
                }
                else{
                    Log.i("#@!", "Weather has not changed!")
                }
            }, {})
            .untilDestroy()
    }

    private fun weatherHasChanged(city: CityCurrentWeather): Boolean {
        return city.main.temp.toInt() != currentWeather!!.main.temp.toInt()
                || city.main.feels_like.toInt() != currentWeather!!.main.feels_like.toInt()
                || city.main.humidity != currentWeather!!.main.humidity
                || city.weather[0].description != currentWeather!!.weather[0].description
    }

    fun loadCity(cityName: String) {
        viewState.setIsCurrentLoading(true)
        getCurrentWeatherUseCase(cityName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate {
                viewState.setIsCurrentLoading(false)
            }
            .subscribe({
                viewState.bindCity(it)
                currentWeather = it
            }, {
                viewState.goBack()
            }).untilDestroy()
    }

    fun onAddToFavoriteButtonClicked(cityName: String, isChecked: Boolean) {
        if (!isChecked) {
            insertFavoriteCityUseCase(FavoriteCity(name = cityName))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    it.printStackTrace()
                })
                .untilDestroy()
        } else {
            deleteFavoriteCityUseCase(cityName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    it.printStackTrace()
                })
                .untilDestroy()
        }
    }

    fun haveInFavorites(cityName: String) {
        getFavoriteCityUseCase(cityName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.setIsInFavorites(true)
            }, {
                viewState.setIsInFavorites(false)
                it.printStackTrace()
            })
            .untilDestroy()
    }

    fun loadForecast(cityName: String) {
        viewState.setIsForecastLoading(true)
        getWeatherForecastUseCase(cityName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate {
                viewState.setIsForecastLoading(false)
            }
            .subscribe({
                viewState.bindForecastList(it.list)
            }, {
                viewState.goBack()
            }).untilDestroy()
    }

    fun goBack() {
        viewState.goBack()
    }

    fun loadWeatherPicture(
        context: Context,
        cityCurrentWeather: CityCurrentWeather,
        imageView: ImageView
    ) {
        val url = "http://openweathermap.org/img/wn/${cityCurrentWeather.weather[0].icon}@2x.png"
        Picasso.with(context).load(url).resize(32, 32).into(imageView)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
        timerForCash.cancel()
    }
}