package com.bignerdranch.android.digest.presentation.news.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.bignerdranch.android.digest.databinding.ActivityNewsDetailsBinding
import com.bignerdranch.android.digest.domain.news.news_model.Article
import com.bignerdranch.android.digest.presentation.news.list.NewsListActivity
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter
import javax.inject.Inject
import javax.inject.Provider

@AndroidEntryPoint
class NewsDetailsActivity : MvpAppCompatActivity(), NewsDetailsView {

    @Inject
    lateinit var presenterProvider: Provider<NewsDetailsPresenter>
    private val newsDetailsPresenter by moxyPresenter { presenterProvider.get() }

    private lateinit var article: Article
    private lateinit var binding: ActivityNewsDetailsBinding

    companion object {
        const val ARTICLE = "article"

        fun start(context: Context, article: Article) {
            val intent = Intent(context, NewsDetailsActivity::class.java).also {
                it.putExtra(ARTICLE, article)
            }
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        article = intent.getSerializableExtra(ARTICLE) as Article
        initViews()
        setContentView(binding.root)
        newsDetailsPresenter.bindArticle(article)
    }

    private fun initViews() {
        binding = ActivityNewsDetailsBinding.inflate(layoutInflater).apply {
            backButtonDetails.setOnClickListener {
                newsDetailsPresenter.openNewsList()
            }
            newsDetailsPresenter.haveInFavorites(article.title)
            addToFavoriteButton.setOnCheckedChangeListener { _, isChecked ->
                newsDetailsPresenter.onAddToFavoritesButtonClicked(article, !isChecked)
            }
        }
    }

    override fun bindArticle(article: Article) {
        binding.apply {
            titleDetails.text = article.title
            Picasso.with(this@NewsDetailsActivity).load(article.urlToImage).into(newsImageDetails)
            newsContentText.text = article.description
        }
    }

    override fun openNewsList() {
        NewsListActivity.start(this)
    }

    override fun setIsInFavorites(isInFavorites: Boolean) {
        binding.addToFavoriteButton.isChecked = isInFavorites
    }
}