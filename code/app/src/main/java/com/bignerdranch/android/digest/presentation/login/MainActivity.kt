package com.bignerdranch.android.digest.presentation.login

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bignerdranch.android.digest.databinding.ActivityMainBinding
import com.bignerdranch.android.digest.presentation.OptionsActivity
import com.bignerdranch.android.digest.presentation.registration.SignUpActivity
import com.google.firebase.auth.FirebaseAuth
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), MainView {
    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var firebaseAuth: FirebaseAuth

    @InjectPresenter
    lateinit var activityMainPresenter: MainPresenter

    companion object{
        private const val LOCATION_CODE = 101
        fun start(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_CODE)
        val currentUser = firebaseAuth.currentUser
        currentUser?.let {
            activityMainPresenter.onUserIsSignIn()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        initViews()
        setContentView(activityMainBinding.root)
    }

    private fun initViews(){
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        activityMainBinding.loginButton.setOnClickListener {
            activityMainPresenter.onLoginButtonClicked(
                activityMainBinding.emailEditText.text.toString(),
                activityMainBinding.passwordEditText.text.toString(),
                firebaseAuth
            )
        }
        activityMainBinding.signUpButton.setOnClickListener {
            activityMainPresenter.onSignUpButtonClicked()
        }
    }

    override fun openOptionsActivity() {
        OptionsActivity.start(this)
    }

    override fun openSignUpActivity() {
        SignUpActivity.start(this)
    }

    override fun showError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(this@MainActivity, permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(permission), requestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@MainActivity, "Permission Granted", Toast.LENGTH_SHORT).show()
            } else {
                finish()
            }
        }
    }

}