package com.bignerdranch.android.digest.domain.weather

import io.reactivex.Completable
import io.reactivex.Single

interface FavoriteCityRepository {
    fun getAllFavoritesCities(): Single<List<FavoriteCity>>
    fun insertFavoriteCity(favoriteCity: FavoriteCity): Completable
    fun deleteFavoriteCity(favoriteCityName: String): Completable
    fun getFavoriteCity(favoriteCityName: String): Single<FavoriteCity>
}