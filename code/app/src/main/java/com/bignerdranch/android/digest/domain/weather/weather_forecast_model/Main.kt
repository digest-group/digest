package com.bignerdranch.android.digest.domain.weather.weather_forecast_model

data class Main(
    val temp_max: Double,
    val temp_min: Double
)