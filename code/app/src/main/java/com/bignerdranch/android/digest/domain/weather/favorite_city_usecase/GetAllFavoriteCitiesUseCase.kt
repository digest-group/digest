package com.bignerdranch.android.digest.domain.weather.favorite_city_usecase

import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import com.bignerdranch.android.digest.domain.weather.FavoriteCityRepository
import io.reactivex.Single

class GetAllFavoriteCitiesUseCase(private val favoriteCityRepository: FavoriteCityRepository) {
    operator fun invoke(): Single<List<FavoriteCity>> = favoriteCityRepository.getAllFavoritesCities()
}