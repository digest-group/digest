package com.bignerdranch.android.digest.domain.news

import com.bignerdranch.android.digest.domain.news.news_model.Article
import io.reactivex.Completable
import io.reactivex.Single

interface FavoriteNewsRepository {

    fun getAllFavoriteNews(): Single<List<Article>>
    fun insertFavoriteNews(favoriteArticle: Article): Completable
    fun deleteFavoriteNews(favoriteArticle: Article): Completable
    fun getFavoriteNews(title: String): Single<Article>

}