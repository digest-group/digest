package com.bignerdranch.android.digest.presentation.registration

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.bignerdranch.android.digest.databinding.ActivitySignUpBinding
import com.bignerdranch.android.digest.presentation.login.MainActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class SignUpActivity : MvpAppCompatActivity(), SignUpView {
    @InjectPresenter
    lateinit var signUpPresenter: SignUpPresenter

    private lateinit var signUpBinding: ActivitySignUpBinding
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var firebaseFirestore: FirebaseFirestore

    companion object{
        fun start(context: Context) {
            val intent = Intent(context, SignUpActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseFirestore = FirebaseFirestore.getInstance()
        initViews()
        setContentView(signUpBinding.root)
    }

    private fun initViews(){
        signUpBinding = ActivitySignUpBinding.inflate(layoutInflater)
        signUpBinding.endSignUpButton.setOnClickListener {
            signUpPresenter.onEndSignUpButtonClicked(signUpBinding, firebaseAuth, firebaseFirestore)
        }
    }

    override fun openLoginActivity() {
        MainActivity.start(this)
    }

    override fun showError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }


}