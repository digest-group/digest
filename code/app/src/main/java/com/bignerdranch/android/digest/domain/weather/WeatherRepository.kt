package com.bignerdranch.android.digest.domain.weather

import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.current_weather_model.Response
import com.bignerdranch.android.digest.domain.weather.weather_forecast_model.CityWeatherForecast
import io.reactivex.Single

interface WeatherRepository {

    fun getCurrentWeather(cityName: String): Single<CityCurrentWeather>
    fun getWeatherForecast(cityName: String): Single<CityWeatherForecast>

    fun getCitiesList(lat: Double, lon: Double): Single<Response>

}