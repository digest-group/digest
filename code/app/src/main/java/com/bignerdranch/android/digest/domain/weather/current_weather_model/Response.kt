package com.bignerdranch.android.digest.domain.weather.current_weather_model

data class Response (
    val list: List<CityCurrentWeather>
)
