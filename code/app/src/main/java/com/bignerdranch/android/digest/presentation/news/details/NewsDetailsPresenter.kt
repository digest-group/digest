package com.bignerdranch.android.digest.presentation.news.details

import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.DeleteFavoriteNewsUseCase
import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.GetFavoriteNewsUseCase
import com.bignerdranch.android.digest.domain.news.favorite_news_usecase.InsertFavoriteNewsUseCase
import com.bignerdranch.android.digest.domain.news.news_model.Article
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class NewsDetailsPresenter @Inject constructor(
    private val insertFavoriteNewsUseCase: InsertFavoriteNewsUseCase,
    private val deleteFavoriteNewsUseCase: DeleteFavoriteNewsUseCase,
    private val getFavoriteNewsUseCase: GetFavoriteNewsUseCase
): MvpPresenter<NewsDetailsView>() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private fun Disposable.untilDestroy() {
        compositeDisposable.add(this)
    }

    fun openNewsList() {
        viewState.openNewsList()
    }

    fun bindArticle(article: Article) {
        viewState.bindArticle(article)
    }

    fun onAddToFavoritesButtonClicked(article: Article, isChecked: Boolean) {
        if (!isChecked) {
            insertFavoriteNewsUseCase(article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    it.printStackTrace()
                })
                .untilDestroy()
        } else {
            deleteFavoriteNewsUseCase(article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    it.printStackTrace()
                })
                .untilDestroy()
        }
    }

    fun haveInFavorites(title: String) {
        getFavoriteNewsUseCase(title)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.setIsInFavorites(true)
            }, {
                viewState.setIsInFavorites(false)
                it.printStackTrace()
            })
            .untilDestroy()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}