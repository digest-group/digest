package com.bignerdranch.android.digest.presentation.adapters.weather

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bignerdranch.android.digest.R
import com.bignerdranch.android.digest.databinding.ItemForecastBinding
import com.bignerdranch.android.digest.domain.weather.weather_forecast_model.Forecast
import com.squareup.picasso.Picasso
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ForecastAdapter: RecyclerView.Adapter<ForecastAdapter.ForecastHolder>() {

    var cityForecast: List<Forecast> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemForecastBinding = ItemForecastBinding.inflate(layoutInflater, parent, false)
        return ForecastHolder(itemForecastBinding)
    }

    override fun onBindViewHolder(holder: ForecastHolder, position: Int) {
        holder.bind(cityForecast[position])
    }

    override fun getItemCount(): Int = cityForecast.count()

    class ForecastHolder(private val itemForecastBinding: ItemForecastBinding) :
        RecyclerView.ViewHolder(itemForecastBinding.root) {
        fun bind(forecast: Forecast) {
            val url = "http://openweathermap.org/img/wn/${forecast.weather[0].icon}@2x.png"
            Picasso.with(itemView.context).load(url).resize(32, 32)
                .into(itemForecastBinding.weatherIcon)

            val pattern = "dd/MM/yyyy HH:mm"
            val simpleDateFormat = SimpleDateFormat(pattern, Locale("en"))
            val date = simpleDateFormat.format(Date(forecast.dt * 1000))
            itemForecastBinding.dateText.text = date.toString()

            if (forecast.main.temp_max.toInt() == forecast.main.temp_min.toInt()){
                itemForecastBinding.minMaxTempText.text = itemView.context.getString(
                    R.string.temp_main_format,
                    (forecast.main.temp_max - 273).toInt().toString()
                )
            } else {
                itemForecastBinding.minMaxTempText.text = itemView.context.getString(
                    R.string.min_max_temp_format,
                    (forecast.main.temp_min - 273).toInt().toString(),
                    (forecast.main.temp_max - 273).toInt().toString()
                )
            }
        }
    }

}