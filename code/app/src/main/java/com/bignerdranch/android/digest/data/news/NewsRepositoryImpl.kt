package com.bignerdranch.android.digest.data.news

import com.bignerdranch.android.digest.domain.news.NewsRepository
import com.bignerdranch.android.digest.domain.news.news_model.BreakingNews
import io.reactivex.Single

class NewsRepositoryImpl(private val newsDataSource: NewsDataSource) : NewsRepository {
    override fun getBreakingNews(): Single<BreakingNews> = newsDataSource.getBreakingNews()
    override fun getEverything(query: String): Single<BreakingNews> = newsDataSource.getEverything(query)
}