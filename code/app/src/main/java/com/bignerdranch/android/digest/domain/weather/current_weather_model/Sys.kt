package com.bignerdranch.android.digest.domain.weather.current_weather_model

data class Sys(
    val country: String
)