package com.bignerdranch.android.digest.presentation.registration

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleTagStrategy::class)
interface SignUpView: MvpView {

    fun showError(message: String)
    fun openLoginActivity()
}