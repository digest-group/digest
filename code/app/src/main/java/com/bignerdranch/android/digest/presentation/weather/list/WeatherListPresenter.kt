package com.bignerdranch.android.digest.presentation.weather.list

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Location
import androidx.core.content.ContextCompat
import com.bignerdranch.android.digest.R
import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import com.bignerdranch.android.digest.domain.weather.current_weather_model.CityCurrentWeather
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.GetAllFavoriteCitiesUseCase
import com.bignerdranch.android.digest.domain.weather.weather_usecase.GetCitiesListUseCase
import com.bignerdranch.android.digest.domain.weather.weather_usecase.GetCurrentWeatherUseCase
import com.google.android.gms.location.LocationServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpAppCompatActivity
import moxy.MvpPresenter
import javax.inject.Inject


@InjectViewState
class WeatherListPresenter @Inject constructor(
    private val getCitiesListUseCase: GetCitiesListUseCase,
    private val getAllFavoriteCitiesUseCase: GetAllFavoriteCitiesUseCase,
    private val getCurrentWeatherUseCase: GetCurrentWeatherUseCase
) : MvpPresenter<WeatherListView>() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private fun Disposable.untilDestroy() {
        compositeDisposable.add(this)
    }

    fun openDetails(cityName: String) {
        if (cityName.isEmpty()) {
            showError("Field is empty!")
        } else {
            viewState.openDetails(cityName)
        }
    }

    fun onCityClicked(cityCurrent: CityCurrentWeather) {
        viewState.openDetails(cityCurrent.name)
    }

    private fun convertFavoriteCitiesList(list: List<FavoriteCity>) {
        list.forEach { favoriteCity ->
            getCurrentWeatherUseCase(favoriteCity.name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.bindOneCity(it)
                }, {
                    it.printStackTrace()
                })
                .untilDestroy()
        }
    }

    fun onFavoritesClicked() {
        viewState.setIsLoading(true)
        viewState.cleanCitiesList()
        getAllFavoriteCitiesUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate {
                viewState.setIsLoading(false)
            }
            .subscribe({
                convertFavoriteCitiesList(it)
            }, {
                it.printStackTrace()
            })
            .untilDestroy()
    }

    fun onCurrentClicked(activity: MvpAppCompatActivity) {

        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            CountingIdlingResourceSingleton.increment()
            viewState.setIsLoading(true)
            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                val latitude: Double
                val longitude: Double
                if (location == null) {
                    viewState.showError("Your location tracker is off.")
                    latitude = 54.0
                    longitude = 83.0
                } else {
                    println("lat = ${location.latitude}  lot = ${location.longitude}")
                    latitude = location.latitude
                    longitude = location.longitude
                }
                getCitiesListUseCase(latitude, longitude)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doAfterTerminate {
                        viewState.setIsLoading(false)
                        CountingIdlingResourceSingleton.decrement()
                    }
                    .subscribe({
                        viewState.bindCitiesList(it.list)
                    }, {
                        it.printStackTrace()
                    })
                    .untilDestroy()
            }
        }

    }

    private fun showError(message: String) {
        viewState.showError(message)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}