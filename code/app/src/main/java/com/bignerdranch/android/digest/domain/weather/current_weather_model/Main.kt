package com.bignerdranch.android.digest.domain.weather.current_weather_model

data class Main(
    val feels_like: Double,
    val humidity: Int,
    val temp: Double
)