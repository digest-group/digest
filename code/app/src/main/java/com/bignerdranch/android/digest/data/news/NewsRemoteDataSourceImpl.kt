package com.bignerdranch.android.digest.data.news

import com.bignerdranch.android.digest.domain.news.news_model.BreakingNews
import io.reactivex.Single

class NewsRemoteDataSourceImpl(private val newsApi: NewsApi): NewsDataSource {
    override fun getBreakingNews(): Single<BreakingNews> = newsApi.getBreakingNews()
    override fun getEverything(query: String): Single<BreakingNews> = newsApi.getEverything(query)
}