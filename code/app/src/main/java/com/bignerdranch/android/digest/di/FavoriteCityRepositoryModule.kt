package com.bignerdranch.android.digest.di

import com.bignerdranch.android.digest.data.favorites.weather.FavoriteCityDao
import com.bignerdranch.android.digest.data.favorites.weather.FavoriteCityDataSource
import com.bignerdranch.android.digest.data.favorites.weather.FavoriteCityLocalDataSource
import com.bignerdranch.android.digest.data.favorites.weather.FavoriteCityRepositoryImpl
import com.bignerdranch.android.digest.domain.weather.FavoriteCityRepository
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.DeleteFavoriteCityUseCase
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.GetAllFavoriteCitiesUseCase
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.GetFavoriteCityUseCase
import com.bignerdranch.android.digest.domain.weather.favorite_city_usecase.InsertFavoriteCityUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class FavoriteCityRepositoryModule {
    @Singleton
    @Provides
    fun providesFavoriteCityLocalDataSource(favoriteCityDao: FavoriteCityDao): FavoriteCityDataSource =
        FavoriteCityLocalDataSource(favoriteCityDao)

    @Singleton
    @Provides
    fun providesFavoriteCityRepository(favoriteCityDataSource: FavoriteCityDataSource):
            FavoriteCityRepository = FavoriteCityRepositoryImpl(favoriteCityDataSource)

    @Singleton
    @Provides
    fun providesGetAllFavoriteCitiesUseCase(favoriteCityRepository: FavoriteCityRepository):
            GetAllFavoriteCitiesUseCase = GetAllFavoriteCitiesUseCase(favoriteCityRepository)

    @Singleton
    @Provides
    fun providesInsertFavoriteCityUseCase(favoriteCityRepository: FavoriteCityRepository):
            InsertFavoriteCityUseCase = InsertFavoriteCityUseCase(favoriteCityRepository)

    @Singleton
    @Provides
    fun providesDeleteFavoriteCityUseCase(favoriteCityRepository: FavoriteCityRepository):
            DeleteFavoriteCityUseCase = DeleteFavoriteCityUseCase(favoriteCityRepository)

    @Singleton
    @Provides
    fun providesGetFavoriteCityUseCase(favoriteCityRepository: FavoriteCityRepository):
            GetFavoriteCityUseCase = GetFavoriteCityUseCase(favoriteCityRepository)


}