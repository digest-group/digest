package com.bignerdranch.android.digest.presentation.login

import com.google.firebase.auth.FirebaseAuth
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {

    fun onLoginButtonClicked(email: String, password: String, firebaseAuth: FirebaseAuth){
        if (!validateFields(email, password)){
            viewState.showError("Please fill all fields!")
            return
        }
        CountingIdlingResourceSingleton.increment()
        firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    viewState.openOptionsActivity()
                } else {
                    viewState.showError("Can't log in.")
                }
                CountingIdlingResourceSingleton.decrement()
            }
    }

    fun onSignUpButtonClicked(){
        viewState.openSignUpActivity()
    }

    fun onUserIsSignIn(){
        viewState.openOptionsActivity()
    }

    private fun validateFields(email: String, password: String): Boolean{
        return email.isNotEmpty() && password.isNotEmpty()
    }

}