package com.bignerdranch.android.digest.presentation.news.list

import com.bignerdranch.android.digest.domain.news.news_model.Article
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleTagStrategy::class)
interface NewsListView: MvpView{
    fun setIsLoading(isLoading: Boolean)
    fun bindNewsList(news: List<Article>)
    fun openNewsDetails(article: Article)
    fun notifyAboutNews()
}