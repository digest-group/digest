package com.bignerdranch.android.digest.presentation.account

data class User(
    val name: String,
    val gender: String,
    val email: String
)
