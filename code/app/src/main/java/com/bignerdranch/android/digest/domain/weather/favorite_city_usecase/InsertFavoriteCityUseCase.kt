package com.bignerdranch.android.digest.domain.weather.favorite_city_usecase

import com.bignerdranch.android.digest.domain.weather.FavoriteCity
import com.bignerdranch.android.digest.domain.weather.FavoriteCityRepository
import io.reactivex.Completable

class InsertFavoriteCityUseCase(private val favoriteCityRepository: FavoriteCityRepository) {
    operator fun invoke(favoriteCity: FavoriteCity): Completable = favoriteCityRepository.insertFavoriteCity(favoriteCity)
}